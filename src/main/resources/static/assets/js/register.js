function onClickRegister() {

    let isTrue = true;
    let username = $('#username').val();
    let phoneNumber = $('#phoneNumber').val();
    let password = $('#password').val();
    let confirmPassword = $('#confirmPassword').val();
    if (!validateEmail(username)) {
        $('#username').addClass("is-invalid");
        $('#username-feedback').text("Invalid email!");
        isTrue = false;
    }
    if (!validatePhoneNumber(phoneNumber)) {
        $('#phoneNumber').addClass("is-invalid");
        $('#phoneNumber-feedback').text("Invalid phoneNumber!");
        isTrue = false;
    }
    if (!validatePassword(password)) {
        $('#password').addClass("is-invalid");
        $('#password-info').addClass("d-none");
        isTrue = false;
    }
    if (!(confirmPassword === password)) {
        $('#confirmPassword').addClass("is-invalid");
        isTrue = false;
    }

    if (isTrue){
        $('#formBtn').removeAttr("type").attr("type", "submit");
    }
}

function existUsername(username) {
    $.ajax({
        type: 'GET',
        url: "/api/auth/register/username/" + username,
        contentType: false,
        processData: false,
        success:function (res){
            if (res){
                $('#username').addClass("is-invalid");
                $('#username-feedback').css("display","inline-block");
                $('#username-feedback').val("Username already exists!");
            }
        }
    })
}



function existPhoneNumber(phoneNumber) {
    $.ajax({
        type: 'GET',
        url: "/api/auth/register/phoneNumber/" + phoneNumber,
        contentType: false,
        processData: false,
        success:function (res){
            if (res){
                $('#phoneNumber').addClass("is-invalid");
                $('#phoneNumber-feedback').css("display","inline-block");
                $('#phoneNumber-feedback').val("Phone number already exists!");
            }
        }
    })
}


$('#username').change(function (e) {
    $('#username').removeClass("is-invalid");
    $('#username-feedback').css("display","none");
    existUsername($('#username').val());
})
$('#phoneNumber').change(function (e) {
    $('#phoneNumber-feedback').css("display","none");
    $('#phoneNumber').removeClass("is-invalid");
    existPhoneNumber($('#phoneNumber').val());
})
$('#password').change(function (e) {
    $('#password').removeClass("is-invalid");
})
$('#confirmPassword').change(function (e) {
    $('#confirmPassword').removeClass("is-invalid");
})

function validateEmail(email) {
    let re = /\S+@\S+\.\S+/;
    return re.test(email);
}

function validatePhoneNumber(phoneNumber) {
    let re = /(?:\+[9]{2}[8][0-9]{2}[0-9]{3}[0-9]{2}[0-9]{2})/;
    return re.test(phoneNumber);
}

function validatePassword(password) {
    let re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,}$/;
    return re.test(password);
}