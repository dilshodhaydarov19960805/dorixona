$('.mobile_tabs').hide();

$('#burg').on('click' , function(){
    $('.mobile_tabs').slideToggle(1000);
});

var $btnTop = $('.btn_top')
$(window).on("scroll", function(){
    if($(window).scrollTop() >= 20){
        $btnTop.fadeIn();
    }else{
        $btnTop.fadeOut();
    }
});

$btnTop.on('click',function (){
    $('html,body').animate({scrollTop:0},500)
});

$('#searchMedicine').on('keyup',function (){
    let kay = $('#searchMedicine').val();
    $('#valueMedicines').empty();
    $.ajax({
        type:"post",
        url:"/medicine/search/"+kay,
        contentType: false,
        processData: false,
        success:function (result){
            for (let i = 0; i < result.length; i++) {
                $('#valueMedicines').append("<tr>\n" +
                    "                            <td>"+result[i].medicineId+"</td>\n" +
                    "                            <td>"+result[i].medicineName+"</td>\n" +
                    "                            <td>"+result[i].numeral+"</td>\n" +
                    "                            <td>"+result[i].types+"</td>\n" +
                    "                            <td>"+result[i].price+"</td>\n" +
                    "                            <td>"+result[i].date+"</td>\n" +
                    "                        </tr>");

            }
        }
    });
});
