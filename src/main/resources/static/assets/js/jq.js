$('.mobile_tabs').hide();

$('#burg').on('click' , function(){
	$('.mobile_tabs').slideToggle(1000);
});

var $btnTop = $('.btn_top')
$(window).on("scroll", function(){
	if($(window).scrollTop() >= 20){
	$btnTop.fadeIn();
	}else{
	$btnTop.fadeOut();
	}
});

$btnTop.on('click',function (){
	$('html,body').animate({scrollTop:0},500)
});

$('#searchMedicine').on('keyup',function (){
	let kay = $('#searchMedicine').val();
	$('#valueMedicines').empty();
	$.ajax({
		type:"post",
		url:"/medicine/search/"+kay,
		contentType: false,
		processData: false,
		success:function (result){
			for (let i = 0; i < result.length; i++) {
				$('#valueMedicines').append("<tr>\n" +
					"                            <td>"+result[i].medicineId+"</td>\n" +
					"                            <td>"+result[i].medicineName+"</td>\n" +
					"                            <td>"+result[i].numeral+"</td>\n" +
					"                            <td>"+result[i].types+"</td>\n" +
					"                            <td>"+result[i].price+"</td>\n" +
					"                            <td>"+result[i].date+"</td>\n" +
					"                            <td class='d-flex flex-wrap justify-content-between text-white'>\n" +
					"                                <a th:href='/medicine/delete/"+result[i].medicineId+"' class=\"btn btn-danger my-1 px-3\">Delete</a>\n" +
					"                                <a th:href='/medicine/edit/"+result[i].medicineId+"' class=\"btn btn-danger my-1 px-4\">Edit</a>\n" +
					"                            </td>\n" +
					"\n" +
					"                        </tr>");

			}
		}
	});
});

$('#searchEmployee').on('keyup',function (){
	let kay = $('#searchEmployee').val();
	$('#valueEmployees').empty();
	$.ajax({
		type:"post",
		url:"/employee/search/"+kay,
		contentType: false,
		processData: false,
		success:function (result){
			for (let i = 0; i < result.length; i++) {
				$('#valueEmployees').append("<tr>\n" +
					"                            <td th:text=\"${employee.id}\">"+result[i].id+"</td>\n" +
					"                            <td th:text=\"${employee.name}\">"+result[i].name+"</td>\n" +
					"                            <td th:text=\"${employee.lastName}\">"+result[i].lastName+"</td>\n" +
					"                            <td th:text=\"${employee.age}\">"+result[i].age+"</td>\n" +
					"                            <td th:text=\"${employee.phoneNumber}\">"+result[i].phoneNumber+"</td>\n" +
					"                            <td th:text=\"${employee.position}\">"+result[i].position+"</td>\n" +
					"                            <td th:text=\"${employee.salary}\">"+result[i].salary+"</td>\n" +
					"                            <td th:text=\"${employee.email}\">"+result[i].email+"</td>\n" +
					"                            <td class=\"d-flex flex-wrap justify-content-between text-white \">\n" +
					"                                <a th:href='/employee/delete/"+result[i].id+"' class=\"btn btn-danger my-1 px-3\">O'chirish</a>\n" +
					"                                <a th:href='/employee/edit/"+result[i].id+"' class=\"btn btn-danger my-1 px-4\">Tahrirlash</a>\n" +
					"                            </td>\n" +
					"\n" +
					"                        </tr>");

			}
		}
	});
});

$('#searchSuggestion').on('keyup',function (){
	let kay = $('#searchSuggestion').val();
	$('#valueSuggestions').empty();
	$.ajax({
		type:"post",
		url:"/suggestion/search/"+kay,
		contentType: false,
		processData: false,
		success:function (result){
			for (let i = 0; i < result.length; i++) {
				$('#valueSuggestions').append("<tr>\n" +
					"                            <td>"+result[i].complaintSuggestionId+"</td>\n" +
					"                            <td>"+result[i].complaintSuggestionText+"</td>\n" +
					"                            <td class='d-flex flex-wrap justify-content-center text-white'>\n" +
					"                             	<a href='/suggestion/delete/"+result[i].complaintSuggestionId+"' class=\"btn btn-danger\">O'chirish</a>"+
					"                            </td>\n" +
					"                        </tr>");

			}
		}
	});
});

// <--------SAVE MEDICINES------->

$('#saveMedicine').on('click',function (e){
	e.preventDefault();
	let name = $('#medicineName').val();
	let numeral = $('#medicineNumeral').val();
	let type = $('#medicineTypes').val();
	let price = $('#medicinePrice').val();
	let date = $('#medicineDate').val();
	if (name !== null && numeral !== null && type !== null && price !== null && date !== null){
		$.ajax({
			type:"POST",
			url:"/medicine/save",
			data:JSON.stringify({
				"medicineName":name,
				"numeral":numeral,
				"types":type,
				"price":price,
				"date":date
			}),
			dataType: "json",
			contentType: "application/json",
			success:function (result){
				if (result.success){
					alert(result.message);
				}else {
					alert(result.message);
				}
			},
			error:function (e){
				alert("error");
			}
		});
	}else {

	}

});