package dorixona.tatu.uz.repository;

import dorixona.tatu.uz.model.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByEmail(String userName);
    boolean existsByEmail(String userName);
    boolean existsByPhoneNumber(String phoneNumber);

}
