package dorixona.tatu.uz.repository;

import dorixona.tatu.uz.model.domain.medicines.Medicines;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicinesRepository extends JpaRepository<Medicines, Long> {
    List<Medicines> findByMedicineNameContainingIgnoreCase(String medicineName);

}
