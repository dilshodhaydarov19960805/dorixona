package dorixona.tatu.uz.repository;

import dorixona.tatu.uz.model.domain.Role;
import dorixona.tatu.uz.model.domain.enurmation.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    Optional<Role> findByName(RoleName roleName);

}
