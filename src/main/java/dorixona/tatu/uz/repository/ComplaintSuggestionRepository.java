package dorixona.tatu.uz.repository;

import dorixona.tatu.uz.model.domain.ComplaintSuggestion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ComplaintSuggestionRepository extends JpaRepository<ComplaintSuggestion, Long> {

    List<ComplaintSuggestion> findByComplaintSuggestionTextContainingIgnoreCase(String complaintSuggestionText);
}
