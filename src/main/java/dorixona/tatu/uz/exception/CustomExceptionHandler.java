package dorixona.tatu.uz.exception;

import dorixona.tatu.uz.model.ResponseData;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

@RestControllerAdvice
public class CustomExceptionHandler {

    @ExceptionHandler(value = UsernameException.class)
    public final ResponseEntity<?> handleUsernameException(UsernameException exception, WebRequest request) {

        return new ResponseEntity<>(new ResponseData<>(
                false, exception.getMessage(), new Date()
        ), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = PhoneNumberException.class)
    public final ResponseEntity<?> handlePhoneNumberException(PhoneNumberException exception, WebRequest request) {
        return new ResponseEntity<>(new ResponseData<>(
                false, exception.getMessage(), new Date()
        ), HttpStatus.BAD_REQUEST);
    }

}
