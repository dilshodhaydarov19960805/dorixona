package dorixona.tatu.uz.controller;

import dorixona.tatu.uz.model.domain.ComplaintSuggestion;
import dorixona.tatu.uz.model.dto.CurrentUser;
import dorixona.tatu.uz.repository.ComplaintSuggestionRepository;
import dorixona.tatu.uz.repository.MedicinesRepository;
import dorixona.tatu.uz.repository.UserRepository;
import dorixona.tatu.uz.security.UserPrincipal;
import dorixona.tatu.uz.service.AuthService;
import dorixona.tatu.uz.service.EmployeeService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class MainController {

    private final ComplaintSuggestionRepository complaintSuggestionRepository;
    private final MedicinesRepository drugRepository;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final AuthService authService;
    private final EmployeeService employeeService;

    public MainController(ComplaintSuggestionRepository complaintSuggestionRepository, MedicinesRepository drugRepository, UserRepository userRepository, PasswordEncoder passwordEncoder, AuthService authService, EmployeeService employeeService) {
        this.complaintSuggestionRepository = complaintSuggestionRepository;
        this.drugRepository = drugRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.authService = authService;
        this.employeeService = employeeService;
    }

    @RequestMapping(value = {"/","index"},method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("medicines",drugRepository.findAll());
        return "index";
    }

    @RequestMapping(value = "/index/suggestion/add",method = RequestMethod.GET)
    public String addToSuggestion() {
        return "addToSuggestion";
    }

    @RequestMapping(value = "/index/suggestion/save",method = RequestMethod.POST)
    public String create(@RequestParam String complaintSuggestionText) {
        if (complaintSuggestionText != null) {
            complaintSuggestionRepository.save(new ComplaintSuggestion(complaintSuggestionText));
            return "medicine";
        } else { return "addToSuggestion";}
    }

    @GetMapping("/login")
    public String signIn(){
        if (authService.loginAuth()){
            return "login";
        }else {
            return "redirect:/cabinet";
        }
    }

    @GetMapping("/cabinet")
    public String getEmployeePage(@CurrentUser UserPrincipal userPrincipal, Model model) {
        model.addAttribute("fullName",userPrincipal.getUser().getFirstName() + " " + userPrincipal.getUser().getLastName());
        model.addAttribute("employees",employeeService.findAll());
        return "cabinet";
    }

    @RequestMapping(value = "/contact",method = RequestMethod.GET)
    public String contact(@CurrentUser UserPrincipal userPrincipal){
        return "contact";
    }

}
