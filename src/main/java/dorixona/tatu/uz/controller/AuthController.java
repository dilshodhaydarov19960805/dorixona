package dorixona.tatu.uz.controller;

import dorixona.tatu.uz.model.ResponseData;
import dorixona.tatu.uz.model.dto.UserDto;
import dorixona.tatu.uz.repository.UserRepository;
import dorixona.tatu.uz.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

@Controller
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private AuthService authService;
    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String signUp(UserDto user, Model model) {
        if (authService.loginAuth()) {
            model.addAttribute("user", user);
            return "registration";
        } else {
            return "redirect:/register";
        }
    }

    @PostMapping(value = "/register")
    public String registerUser( @ModelAttribute("user") UserDto userDto, BindingResult result, HttpServletRequest request) {
        if (result.hasErrors()){
            return "registration";
        }else {
            ResponseData responseData = authService.registerUser(userDto, Locale.forLanguageTag("uz"));
            if (responseData.isSuccess()){
                UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(userDto.getEmail(), userDto.getPassword());
                token.setDetails(new WebAuthenticationDetails(request));
                Authentication authentication = authenticationManager.authenticate(token);
                SecurityContextHolder.getContext().setAuthentication(authentication);
                return "redirect:/cabinet";
            }
        }
        return "registration";
    }

    @GetMapping("/register/username/{username}")
    public ResponseEntity<?> checkUsername(@PathVariable String username){
        return ResponseEntity.ok(userRepository.existsByEmail(username));
    }

    @GetMapping("/register/phoneNumber/{phoneNumber}")
    public ResponseEntity<?> checkPhoneNumber(@PathVariable String phoneNumber){
        return ResponseEntity.ok(userRepository.existsByPhoneNumber(phoneNumber));
    }

}
