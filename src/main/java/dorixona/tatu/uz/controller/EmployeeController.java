package dorixona.tatu.uz.controller;

import dorixona.tatu.uz.model.domain.Employee;
import dorixona.tatu.uz.model.dto.CurrentUser;
import dorixona.tatu.uz.repository.EmployeeRepository;
import dorixona.tatu.uz.security.UserPrincipal;
import dorixona.tatu.uz.service.EmployeeService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
@RequestMapping("/employee")
public class EmployeeController {

    private final EmployeeRepository employeeRepository;
    private final EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService, EmployeeRepository employeeRepository) {
        this.employeeService = employeeService;
        this.employeeRepository = employeeRepository;
    }


    @PostMapping("/save")
    public String create(@RequestParam String name,
                         String lastName, Long age, String phoneNumber, String position, Long salary, String email, Model model, @CurrentUser UserPrincipal userPrincipal) {
        if (name !=null && lastName != null && age != null && phoneNumber != null
                && position != null && salary != null && email != null && 9 <= email.length()){
            employeeService.save(new Employee(name,lastName,age,phoneNumber,position,salary,email));
            model.addAttribute("employees",employeeService.findAll());
            model.addAttribute("fullName",userPrincipal.getUser().getFirstName() + " " + userPrincipal.getUser().getLastName());
            return "employee";
        }
        else {
            return "addToEmployee";
        }
    }

    @GetMapping("/add")
    public String openModel(@CurrentUser UserPrincipal userPrincipal, Model model) {
        model.addAttribute("fullName",userPrincipal.getUser().getFirstName() + " " + userPrincipal.getUser().getLastName());
        return "addToEmployee";
    }

    @GetMapping
    public String getEmployeePage(Model model) {
        model.addAttribute("employees",employeeService.findAll());
        return "employee";
    }

    @PostMapping("/search/{kay}")
    @ResponseBody
    public List<Employee> findByName(@PathVariable("kay") String kay){
        return employeeRepository.findByNameContainingIgnoreCaseOrLastNameContainingIgnoreCase(kay,kay);
    }

    @PostMapping("/update/{id}")
    public String update(@PathVariable("id") Long id, Model model, Employee employee ,
                         BindingResult result, @CurrentUser UserPrincipal userPrincipal) {
        if (result.hasErrors()){
            employee.setId(id);
            return "updateToEmployee";
        }
        employeeService.save(employee);
        model.addAttribute("fullName",userPrincipal.getUser().getFirstName() + " " + userPrincipal.getUser().getLastName());
        model.addAttribute("employees",employeeService.findAll());
        return "employee";
    }

    @GetMapping(value = "/delete/{id}")
    public String delete(@PathVariable(name = "id") Long id, Model model) {
        Employee employee = employeeRepository.findById(id).orElseThrow(()->
                new IllegalArgumentException("Invalid employee id: " + id));
        employeeRepository.delete(employee);
        model.addAttribute("employees", employeeService.findAll());
        return "employee";
    }

    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") Long id, Model model, @CurrentUser UserPrincipal userPrincipal){
        Employee employee =employeeRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid employee id: " + id));
        model.addAttribute("employee",employee);
        model.addAttribute("fullName",userPrincipal.getUser().getFirstName() + " " + userPrincipal.getUser().getLastName());
        return "updateToEmployee";
    }

    //    @GetMapping("/employees/{name}")
//    public ResponseEntity findByName(@PathVariable String name) {
//        Optional<String> optional = SecurityCurrentUtils.getCurrentUserName();
//        List<Employee> employeeList = employeeService.findByName(name);
//        return ResponseEntity.ok(employeeList);
//    }

}
