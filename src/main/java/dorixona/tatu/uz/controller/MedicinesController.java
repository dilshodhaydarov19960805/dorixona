package dorixona.tatu.uz.controller;

import dorixona.tatu.uz.model.domain.medicines.Medicines;
import dorixona.tatu.uz.model.dto.MedicineDto;
import dorixona.tatu.uz.repository.MedicinesRepository;
import dorixona.tatu.uz.service.MedicinesService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@Controller
@RequestMapping("/medicine")
public class MedicinesController {

    private final MedicinesService medicinesService;
    private final MedicinesRepository medicinesRepository;

    public MedicinesController(MedicinesService medicinesService, MedicinesRepository medicinesRepository) {
        this.medicinesService = medicinesService;
        this.medicinesRepository = medicinesRepository;
       }

    @PostMapping("/save")
    public ResponseEntity<?> create(@RequestBody MedicineDto medicineDto) {
        return medicinesService.save(medicineDto);
    }

    @GetMapping("/add")
    public String openModel(){
        return "addToMedicine";
    }

    @GetMapping
    public String getMedicinePage(Model model) {
        model.addAttribute("medicines", medicinesService.findAll());
        return "medicine";
    }

    @PostMapping("/search/{kay}")
    @ResponseBody
    public List<Medicines> findByName(@PathVariable("kay") String kay){
        return medicinesRepository.findByMedicineNameContainingIgnoreCase(kay);
    }

    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") Long id, Model model){
        Medicines medicines = medicinesRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid medicine id: " + id));
        model.addAttribute("medicine",medicines);
        return "updateToMedicines";
    }

    @PostMapping("/update/{id}")
    public String update(@PathVariable("id") Long id, Model model, Medicines medicines,
                         BindingResult result) {
        if (result.hasErrors()){
            medicines.setMedicineId(id);
            return "updateToMedicines";
        }
        medicines.setMedicineId(id);
        medicinesService.update(medicines);
        model.addAttribute("medicines", medicinesService.findAll());
        return "medicine";
    }

    @GetMapping(value = "/delete/{id}")
    public String delete(@PathVariable(name = "id") Long id, Model model) {
        Medicines medicines = medicinesRepository.findById(id).orElseThrow(()->
                new IllegalArgumentException("Invalid drug id: " + id));
        medicinesRepository.delete(medicines);
        model.addAttribute("medicines", medicinesService.findAll());
        return "medicine";
    }

}
