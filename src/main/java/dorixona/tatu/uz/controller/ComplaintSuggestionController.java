package dorixona.tatu.uz.controller;

import dorixona.tatu.uz.model.domain.ComplaintSuggestion;
import dorixona.tatu.uz.model.dto.CurrentUser;
import dorixona.tatu.uz.repository.ComplaintSuggestionRepository;
import dorixona.tatu.uz.security.UserPrincipal;
import dorixona.tatu.uz.service.ComplaintSuggestionService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
@RequestMapping("/suggestion")
public class ComplaintSuggestionController {

    private final ComplaintSuggestionService complaintSuggestionService;
    private final ComplaintSuggestionRepository complaintSuggestionRepository;

    public ComplaintSuggestionController(ComplaintSuggestionService complaintSuggestionService, ComplaintSuggestionRepository complaintSuggestionRepository) {
        this.complaintSuggestionService = complaintSuggestionService;
        this.complaintSuggestionRepository = complaintSuggestionRepository;
    }

    @GetMapping
    public String findAllText(Model model, @CurrentUser UserPrincipal userPrincipal) {
        model.addAttribute("suggestions",complaintSuggestionService.findAll());
        model.addAttribute("fullName",userPrincipal.getUser().getFirstName() + " " + userPrincipal.getUser().getLastName());
        return "suggestion";
    }

    @PostMapping("/search/{kay}")
    @ResponseBody
    public List<ComplaintSuggestion> findByName(@PathVariable("kay") String kay){
        return complaintSuggestionRepository.findByComplaintSuggestionTextContainingIgnoreCase(kay);
    }

    @GetMapping(value = "/delete/{id}")
    public String delete(@PathVariable(name = "id") Long id, Model model) {
        ComplaintSuggestion complaintSuggestion = complaintSuggestionRepository.findById(id).orElseThrow(()->
                new IllegalArgumentException("Invalid drug id: " + id));
        complaintSuggestionRepository.delete(complaintSuggestion);
        model.addAttribute("suggestions", complaintSuggestionService.findAll());
        return "suggestion";
    }

}
