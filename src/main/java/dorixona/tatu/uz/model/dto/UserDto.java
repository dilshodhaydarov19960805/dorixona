package dorixona.tatu.uz.model.dto;

import dorixona.tatu.uz.model.domain.User;
import dorixona.tatu.uz.model.domain.enurmation.Status;
import org.springframework.security.core.GrantedAuthority;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.List;

public class UserDto {

    private Long id;
    @NotBlank(message = "username not blank")
    @Email(message = "Wrong email")
    private String email;

    @NotBlank(message = "password not blank")
    @Pattern(regexp = "((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{6,20})", message = "Not allowed password")
    private String password;

    @NotBlank(message = "phoneNumber not blank")
    @Pattern(regexp = "^[+][9][9][8][0-9]{9}$", message = "Wrong phone number")
    private String phoneNumber;
    private String firstName;
    private String lastName;
    private Status status;
    private List<GrantedAuthority> authorities;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public User map2Entity() {
        User user = new User();
        user.setEmail(email);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setPhoneNumber(phoneNumber);
        user.setStatus(status);
        user.setPassword(password);
        return user;
    }
}
