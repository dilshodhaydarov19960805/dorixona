package dorixona.tatu.uz.model.dto;


import dorixona.tatu.uz.model.domain.medicines.Medicines;

public class MedicineDto {

    private Long medicineId;
    private String medicineName;
    private Long price;
    private Long numeral;
    private String date;
    private String types;

    public Long getMedicineId() {
        return medicineId;
    }

    public void setMedicineId(Long medicineId) {
        this.medicineId = medicineId;
    }

    public String getMedicineName() {
        return medicineName;
    }

    public void setMedicineName(String medicineName) {
        this.medicineName = medicineName;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Long getNumeral() {
        return numeral;
    }

    public void setNumeral(Long numeral) {
        this.numeral = numeral;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTypes() {
        return types;
    }

    public void setTypes(String types) {
        this.types = types;
    }

    public Medicines map2Entity() {
        Medicines medicines  = new Medicines();
        medicines.setMedicineName(medicineName);
        medicines.setDate(date);
        medicines.setNumeral(numeral);
        medicines.setTypes(types);
        medicines.setPrice(price);
        return medicines;
    }
}
