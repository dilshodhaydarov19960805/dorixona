package dorixona.tatu.uz.model;

import java.util.Date;
import java.util.Map;

public class ResponseData<T> {
    private boolean success;
    private String message;
    private T data;
    private Map<String,String> errors;
    private Date timestamp;

    public ResponseData(boolean success, String message,Date timestamp) {
        this.success = success;
        this.message = message;
        this.timestamp=timestamp;
    }

    public ResponseData() {
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Map<String, String> getErrors() {
        return errors;
    }

    public void setErrors(Map<String, String> errors) {
        this.errors = errors;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
}
