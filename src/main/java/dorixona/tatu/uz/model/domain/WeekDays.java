package dorixona.tatu.uz.model.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class WeekDays implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "week_days_id")
    private Long weekDaysId;
    @Column(name = "week_day_name")
    private String weekDayName;

    public Long getWeekDaysId() {
        return weekDaysId;
    }
    public void setWeekDaysId(Long weekDaysId) {
        this.weekDaysId = weekDaysId;
    }
    public String getWeekDayName() {
        return weekDayName;
    }
    public void setWeekDayName(String weekDayName) {
        this.weekDayName = weekDayName;
    }

}

