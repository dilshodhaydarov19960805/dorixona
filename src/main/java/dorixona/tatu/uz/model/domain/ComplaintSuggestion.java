package dorixona.tatu.uz.model.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class ComplaintSuggestion implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "complaint_suggestion_id")
    private Long complaintSuggestionId;
    @Column(name = "complaint_suggestion_text")

    private String complaintSuggestionText;

    public ComplaintSuggestion(String complaintSuggestionText) {
        this.complaintSuggestionText = complaintSuggestionText;
    }

    public ComplaintSuggestion() {
    }

    public Long getComplaintSuggestionId() {
        return complaintSuggestionId;
    }

    public void setComplaintSuggestionId(Long complaintSuggestionId) {
        this.complaintSuggestionId = complaintSuggestionId;
    }

    public String getComplaintSuggestionText() {
        return complaintSuggestionText;
    }

    public void setComplaintSuggestionText(String complaintSuggestionText) {
        this.complaintSuggestionText = complaintSuggestionText;
    }

}

