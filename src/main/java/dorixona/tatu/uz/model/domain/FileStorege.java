package dorixona.tatu.uz.model.domain;

import dorixona.tatu.uz.model.domain.enurmation.FileStoregeStatus;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class FileStorege implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private  String extention;
    private Long fileSize;
    private String hashId;
    private String contentype;
    private String uploadPath;
    @Enumerated(EnumType.STRING )
    private FileStoregeStatus fileStoregeStatus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExtention() {
        return extention;
    }

    public void setExtention(String extention) {
        this.extention = extention;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getHashId() {
        return hashId;
    }

    public void setHashId(String hashId) {
        this.hashId = hashId;
    }

    public String getContentype() {
        return contentype;
    }

    public void setContentype(String contentype) {
        this.contentype = contentype;
    }

    public String getuploadPath() {
        return uploadPath;
    }

    public void setuploadPath(String uploadPath) {
        this.uploadPath = uploadPath;
    }

    public FileStoregeStatus getFileStoregeStatus() {
        return fileStoregeStatus;
    }

    public void setFileStoregeStatus(FileStoregeStatus fileStoregeStatus) {
        this.fileStoregeStatus = fileStoregeStatus;
    }

}
