package dorixona.tatu.uz.model.domain.medicines;

import com.sun.istack.NotNull;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class MedicinesDepartment implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "medicines_department_id")
    private Long medicinesDepartmentId;
    @NotNull
    @Column(name = "medicines_department_name")
    private String medicinesDepartmentName;

    public Long getMedicinesDepartmentId() {
        return medicinesDepartmentId;
    }

    public void setMedicinesDepartmentId(Long medicinesDepartmentId) {
        this.medicinesDepartmentId = medicinesDepartmentId;
    }

    public String getMedicinesDepartmentName() {
        return medicinesDepartmentName;
    }

    public void setMedicinesDepartmentName(String medicinesDepartmentName) {
        this.medicinesDepartmentName = medicinesDepartmentName;
    }
}
