package dorixona.tatu.uz.model.domain.medicines;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "medicines")
public class Medicines implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "medicine_id")
    private Long medicineId;

    @Column(name = "medicine_name")
    private String medicineName;

    @Column(name = "price")
    private Long price;
    @Column(name = "numeral")
    private Long numeral;
    @Column(name = "date")
    private String date;
    private String types;
    @ManyToMany
    @JoinTable(
            name = "medicine_department_medicines",
            joinColumns = {@JoinColumn(name = "id",referencedColumnName = "medicine_id")},
            inverseJoinColumns = {@JoinColumn(name = "name_medicines",referencedColumnName = "medicines_department_name")}

    )
    private Set<MedicinesDepartment> medicinesDepartments = new HashSet<>();

    public Medicines(){ }

    public Long getMedicineId() {
        return medicineId;
    }

    public void setMedicineId(Long medicineId) {
        this.medicineId = medicineId;
    }

    public String getMedicineName() {
        return medicineName;
    }

    public void setMedicineName(String medicineName) {
        this.medicineName = medicineName;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Long getNumeral() {
        return numeral;
    }

    public void setNumeral(Long numeral) {
        this.numeral = numeral;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTypes() {
        return types;
    }

    public void setTypes(String types) {
        this.types = types;
    }

    public Set<MedicinesDepartment> getMedicinesDepartments() {
        return medicinesDepartments;
    }

    public void setMedicinesDepartments(Set<MedicinesDepartment> medicinesDepartments) {
        this.medicinesDepartments = medicinesDepartments;
    }

}
