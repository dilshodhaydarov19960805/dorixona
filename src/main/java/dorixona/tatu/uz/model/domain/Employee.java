package dorixona.tatu.uz.model.domain;

import com.sun.istack.NotNull;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "employee")
public class Employee implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "first_name")
    private String name;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "yoshi")
    private Long age;
    @Column(name = "tel_raqami")
    private String phoneNumber;
    @NotNull
    @Column(name = "lavozimi")
    private String position;
    @NotNull
    @Column(name = "maoshi")
    private Long salary;
    private String email;
    @ManyToMany
    @JoinTable(
            name = "employee_week_day",
            joinColumns = {@JoinColumn(name = "employee_week_day_id",referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "employee_week_day_name",referencedColumnName = "week_day_name")}
    )

    private Set<WeekDays> weekDays = new HashSet<>();

    public Employee(String name, String lastName, Long age, String phoneNumber, String position,
                     Long salary , String email) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.phoneNumber = phoneNumber;
        this.position = position;
        this.salary = salary;
        this.email = email;
    }

    public Employee() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<WeekDays> getWeekDays() {
        return weekDays;
    }

    public void setWeekDays(Set<WeekDays> weekDays) {
        this.weekDays = weekDays;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Long getSalary() {
        return salary;
    }

    public void setSalary(Long salary) {
        this.salary = salary;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {

        this.lastName = lastName;
    }

}
