package dorixona.tatu.uz.model.domain.enurmation;

public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
