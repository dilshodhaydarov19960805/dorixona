package dorixona.tatu.uz.service;

import dorixona.tatu.uz.model.domain.Employee;
import dorixona.tatu.uz.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {
    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }
    public Employee save(Employee employee){
        return employeeRepository.save(employee);
    }
    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }

    public List<Employee> findByNameOrLastName(String name, String lastName){
        return employeeRepository.findByNameContainingIgnoreCaseOrLastNameContainingIgnoreCase(name,lastName);
    }

    public void delete(Long id){
        Employee employee = employeeRepository.getOne(id);
        employeeRepository.delete(employee);
    }

//    public List<Employee> findByNamedLastName(String name, String lastName){
//        return employeeRepository.findByNameAndLastName(name, lastName);

//    }
}
