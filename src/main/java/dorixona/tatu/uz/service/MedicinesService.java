package dorixona.tatu.uz.service;

import dorixona.tatu.uz.model.Result;
import dorixona.tatu.uz.model.domain.medicines.Medicines;
import dorixona.tatu.uz.model.dto.MedicineDto;
import dorixona.tatu.uz.repository.MedicinesRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MedicinesService {
    private final MedicinesRepository medicinesRepository;

    public MedicinesService(MedicinesRepository medicinesRepository) {
        this.medicinesRepository = medicinesRepository;
    }

    public ResponseEntity<?> save(MedicineDto medicineDto){
        Result result = new Result();
        if (true){
            Medicines medicines  = medicineDto.map2Entity();
            medicinesRepository.save(medicines);
            result.setStatus(true);
            result.setMessage("Successfully!");
            return ResponseEntity.ok(result);
        }
        result.setStatus(false);
        result.setMessage("Error!");
        return new ResponseEntity(result, HttpStatus.ALREADY_REPORTED);

    }
    public List<Medicines> findAll() {
        return medicinesRepository.findAll();
    }
    public List<Medicines> findByName(String name){
        return medicinesRepository.findByMedicineNameContainingIgnoreCase(name);
    }
    public void delete(Long id){
        Medicines medicines = medicinesRepository.getOne(id);
        medicinesRepository.delete(medicines);
    }

    public Medicines update(Medicines medicineDto){
        return medicinesRepository.save(medicineDto);
    }

//    public List<Medicines> findAllByLike(String name) {
//        return drugRepository.findByDrugNameStartingWith(name);
//    }

    //    public Medicines findById(Long id){
//        return drugRepository.findById(id).get();
//    }

}
