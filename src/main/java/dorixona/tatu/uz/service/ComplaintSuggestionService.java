package dorixona.tatu.uz.service;

import dorixona.tatu.uz.model.domain.ComplaintSuggestion;
import dorixona.tatu.uz.repository.ComplaintSuggestionRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ComplaintSuggestionService {
    private final ComplaintSuggestionRepository complaintSuggestionRepository;

    public ComplaintSuggestionService(ComplaintSuggestionRepository complaintSuggestionRepository) {
        this.complaintSuggestionRepository = complaintSuggestionRepository;
    }

    public ComplaintSuggestion save(ComplaintSuggestion complaintSuggestion){
        return complaintSuggestionRepository.save(complaintSuggestion);
    }
    public List<ComplaintSuggestion> findSearch(String complaintSuggestionText){
        return complaintSuggestionRepository.findByComplaintSuggestionTextContainingIgnoreCase(complaintSuggestionText);
    }
    public List<ComplaintSuggestion> findAll(){
        return complaintSuggestionRepository.findAll();
    }

}
