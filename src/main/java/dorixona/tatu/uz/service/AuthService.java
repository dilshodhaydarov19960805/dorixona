package dorixona.tatu.uz.service;

import dorixona.tatu.uz.exception.PhoneNumberException;
import dorixona.tatu.uz.exception.UsernameException;
import dorixona.tatu.uz.model.ResponseData;
import dorixona.tatu.uz.model.dto.UserDto;
import dorixona.tatu.uz.model.domain.User;
import dorixona.tatu.uz.repository.RoleRepository;
import dorixona.tatu.uz.repository.UserRepository;
import dorixona.tatu.uz.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Service
public class AuthService implements UserDetailsService{

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private RoleRepository roleRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User users = userRepository.findByEmail(s).orElseThrow(()-> new UsernameNotFoundException(s));

        List<GrantedAuthority> authorities = new ArrayList<>();
        return new UserPrincipal(
                users.getEmail(),
                users.getPassword(),
                authorities,
                users
        );
    }

    public ResponseData registerUser(UserDto userDto, Locale locale) {

        PasswordEncoder passwordEncoder  =new BCryptPasswordEncoder();
        ResponseData<Long> response = new ResponseData<>();
        if (userRepository.existsByEmail(userDto.getEmail())) {
            throw new UsernameException(messageSource.getMessage("auth.username.exists", null, locale));
        }
        if (userRepository.existsByPhoneNumber(userDto.getPhoneNumber())) {
            throw new PhoneNumberException(messageSource.getMessage("auth.phoneNumber.exists", null, locale));
        }

        User user = userDto.map2Entity();
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));

        user.setRoles(
                roleRepository.findAll().stream().filter(role ->
                        role.getName().equals("ROLE_USER")).collect(Collectors.toSet()));
        User saved = userRepository.save(user);
        response.setSuccess(true);
        response.setTimestamp(new Date(System.currentTimeMillis()));
        response.setData(saved.getId());
        response.setMessage(messageSource.getMessage("auth.register.success", null, locale));
        return response;
    }

    public boolean loginAuth() {
        return (SecurityContextHolder.getContext().getAuthentication()
                instanceof AnonymousAuthenticationToken);
    }

}
